<?php 
    include "../connection.php";

    $sql = "SELECT * FROM arabica";

    $result = $connect->query($sql);
    
    if($result->num_rows > 0){
       $arabica = array();
       while($row = $result->fetch_assoc()){
        $arabica[] = $row;
       }

       echo json_encode(array(
        "success"=> true,
        "arabica"=>$arabica,
       ));
    }else{
    echo json_encode(array(
        "success" => false,
    ));
    }
?>

    
