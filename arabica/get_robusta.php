<?php 
    include "../connection.php";

    $sql = "SELECT * FROM robusta";

    $result = $connect->query($sql);
    
    if($result->num_rows > 0){
       $robusta = array();
       while($row = $result->fetch_assoc()){
        $robusta[] = $row;
       }

       echo json_encode(array(
        "success"=> true,
        "robusta"=>$robusta,
       ));
    }else{
    echo json_encode(array(
        "success" => false,
    ));
    }
?>

    
